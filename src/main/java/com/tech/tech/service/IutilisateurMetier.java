package com.tech.tech.service;

import java.util.List;

import com.tech.tech.entites.Utilisateur;
public interface IutilisateurMetier {
	public String ajouterUtilisateur(Utilisateur u);
	public void modifierUtilisateur(Utilisateur u);
	public void supprimerUtilisateur(String referenceProduit);
	public Utilisateur getUtilisateur(String referenceProduitr);
	public  List<Utilisateur> listeUtilisateurs();
}
