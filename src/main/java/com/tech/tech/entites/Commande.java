package com.tech.tech.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Commande implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
 private String nCommande;
 private String dateCommande;
 private String typeCommande;
 private String typeOperation;
 
public String getnCommande() {
	return nCommande;
}
public void setnCommande(String nCommande) {
	this.nCommande = nCommande;
}
public String getDateCommande() {
	return dateCommande;
}
public void setDateCommande(String dateCommande) {
	this.dateCommande = dateCommande;
}
public String getTypeCommande() {
	return typeCommande;
}
public void setTypeCommande(String typeCommande) {
	this.typeCommande = typeCommande;
}
public String getTypeOperation() {
	return typeOperation;
}
public void setTypeOperation(String typeOperation) {
	this.typeOperation = typeOperation;
}
public Commande(String nCommande, String dateCommande, String typeCommande, String typeOperation) {
	super();
	this.nCommande = nCommande;
	this.dateCommande = dateCommande;
	this.typeCommande = typeCommande;
	this.typeOperation = typeOperation;
}
public Commande() {
	super();
	// TODO Auto-generated constructor stub
}
 
 
}
