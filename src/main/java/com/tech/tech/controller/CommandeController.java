package com.tech.tech.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tech.tech.entites.Commande;
import com.tech.tech.service.CommandeManager;

@Controller
@RequestMapping(value="adminCommande")
public class CommandeController {
	@Autowired
	CommandeManager commandeManager;
	
	@RequestMapping(value= "/ajout")
	public String home(Model model){
		model.addAttribute("commande",new Commande());
		return "addCommande";
	}
	
	@RequestMapping(value="/saveCommande")
	public String ajoutCommande(@Valid Commande c,BindingResult bind,Model model)
	{
		
		if(bind.hasErrors())
		{
	
			return "addCommande";
		}
		if((c.getnCommande())!=null){
			commandeManager.modifierCommande(c);
			
			
			}
		else
		
			commandeManager.ajouterCommande(c); 
			model.addAttribute("listeCommande",commandeManager.listeCommande());
			
		
		return "listeCommande";
	}
	
	/**********Suppression fournisseurs****************/
	
	@RequestMapping(value="/supprimerCommande")
	public String suppPro(String refCommande,Model model)
	{  try{ 
		commandeManager.supprimerCommande(refCommande);
	}catch(Exception e){}
   	model.addAttribute("listeCommande",commandeManager.listeCommande());
	return "listeCommande";

	}
	
	/******Affichage liste des clients************/
	@RequestMapping(value="liste")
	public String accueil(Model model)
	{
		model.addAttribute("listeCommande",commandeManager.listeCommande());
		return "listeCommande";
	}
	
	@RequestMapping(value="/EditerCommande")
	public String EditerCommande(String refCommande,Model model)
	{  
	Commande ca=commandeManager.getCommande(refCommande);
		model.addAttribute("commande",ca);
	return "addCommande";

	}
	
}
