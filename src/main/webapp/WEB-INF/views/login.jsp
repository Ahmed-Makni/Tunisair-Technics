<%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1" --%>
<%--     pageEncoding="ISO-8859-1"%> --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="j"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>login</title>

<link href="<%=request.getContextPath()%>/resources/css/style.css"
	rel="stylesheet" />
	
<link rel="icon" type="image/png"
	href="<%=request.getContextPath()%>/resources/img/favicon.png" />
</head>
<body>
	<div class="login-page">
		<div class="form">
			<img src="<%=request.getContextPath()%>/resources/img/logo.png"
				class="logo" />

			<form action="<c:url value="/j_spring_security_check"></c:url>" method="POST"
				class="login-form">
				<div class="body bg-gray">
					<div class="form-group">
						<input type="text" name="j_username" class="form-control"
							placeholder="username" />
					</div>
					<div class="form-group">
						<input type="password" name="j_password" class="form-control"
							placeholder="password" />
					</div>

				</div>
				<div class="footer">
					<button type="submit" class="btn bg-olive btn-block">login</button>

				</div>
			</form>
		</div>
	</div>
</body>
</body>
</html>