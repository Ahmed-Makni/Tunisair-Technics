<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Accueil</title>
  <!-- BOOTSTRAP STYLES-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <!-- <link href="assets/css/font-awesome.css" rel="stylesheet" /> -->
        <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>
<body>
     
           
          
    <div id="wrapper">
         <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="adjust-nav">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img src="assets/img/logo.png" />

                    </a>
                    
                </div>

             
                <span class="logout-spn" >
                  <a href="#" style="color:#fff;">LOGOUT</a>  

                </span>
            </div>
        </div>
        <!-- /. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                 <ul class="main-nav-ul">
    <li><a href="#Menuu" class="selected">Menu<span class="sub-arrow"></span></a>
            <ul>
          <li><a href="#">Acceuil</a></li>
            <li><a href="#">Profil</a></li>
            <li><a href="#">Calendrier</a></li>
            <li><a href="#">Liste des utilisateurs</a></li>
            <li><a href="#">Deconnexion</a></li>

         </ul>
         </li>
      <li><a href="#Administration">Administration<span class="sub-arrow"></span></a>
        <ul>
          <li><a href="#">Gestion des utilisateur</a></li>
            <li><a href="#">Controle des acc�s</a></li>
            <li><a href="#">Nouveau mot de passe</a></li>
         </ul>
      </li>
      <li><a href="#Compte">Compte<span class="sub-arrow"></span></a>
        <ul>
          <li><a href="#">Deconnexion</a></li>
         </ul>
         </li>
      <li><a href="#Navigationn">Navigation</a></li>
      <li><a href="#Login">Login</a></li>
   </ul>


                            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-lg-12">
                     <h2>ADMIN DASHBOARD</h2>   
                    </div>
                </div>              
                 <!-- /. ROW  -->
                 
        </div>
        </div>
    <div class="footer">
      
    
            <div class="row">
                <div class="col-lg-12" >
                    &copy;  2017 TunisairTechnics.com | Design by: Makni_Ahmed //makni.ahmed@outlook.com
                </div>
            </div>
        </div>
          

     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
