package com.tech.tech.entites;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * The persistent class for the contrat database table.
 * 
 */
@Entity
@Table(name = "contrat")
@NamedQuery(name = "Contrat.findAll", query = "SELECT c FROM Contrat c")
public class Contrat implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(unique = true, nullable = false, length = 254)
	private String nDeContrat;

	@Column(nullable = false, length = 254)
	private String codetiers;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	private Date dateDeDebutDeContrat;

	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateDeFinDeContrat;

	@Column(nullable = false, length = 50)
	private String delaiAOG;

	@Column(nullable = false, length = 50)
	private String delaiCritique;

	@Column(nullable = false, length = 50)
	private String delaiNormal;

	@Column(nullable = false, length = 50)
	private String designation;

	@Column(length = 254)
	private String libelleContrat;

	@Column(length = 254)
	private String montantDuContrat;

	@Column(length = 255)
	private String nAdresseEnvoiDeCommande;

	@Column(nullable = false, length = 50)
	private String pn;

	@Column(length = 254)
	private String tauxDeRemiseAccordee;

	@Column(name = "`Type de Travail`", nullable = false, length = 50)
	private String type_de_Travail;

	public String getnDeContrat() {
		return nDeContrat;
	}

	public void setnDeContrat(String nDeContrat) {
		this.nDeContrat = nDeContrat;
	}

	public String getCodetiers() {
		return codetiers;
	}

	public void setCodetiers(String codetiers) {
		this.codetiers = codetiers;
	}

	public Date getDateDeDebutDeContrat() {
		return dateDeDebutDeContrat;
	}

	public void setDateDeDebutDeContrat(Date dateDeDebutDeContrat) {
		this.dateDeDebutDeContrat = dateDeDebutDeContrat;
	}

	public Date getDateDeFinDeContrat() {
		return dateDeFinDeContrat;
	}

	public void setDateDeFinDeContrat(Date dateDeFinDeContrat) {
		this.dateDeFinDeContrat = dateDeFinDeContrat;
	}

	public String getDelaiAOG() {
		return delaiAOG;
	}

	public void setDelaiAOG(String delaiAOG) {
		this.delaiAOG = delaiAOG;
	}

	public String getDelaiCritique() {
		return delaiCritique;
	}

	public void setDelaiCritique(String delaiCritique) {
		this.delaiCritique = delaiCritique;
	}

	public String getDelaiNormal() {
		return delaiNormal;
	}

	public void setDelaiNormal(String delaiNormal) {
		this.delaiNormal = delaiNormal;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getLibelleContrat() {
		return libelleContrat;
	}

	public void setLibelleContrat(String libelleContrat) {
		this.libelleContrat = libelleContrat;
	}

	public String getMontantDuContrat() {
		return montantDuContrat;
	}

	public void setMontantDuContrat(String montantDuContrat) {
		this.montantDuContrat = montantDuContrat;
	}

	public String getnAdresseEnvoiDeCommande() {
		return nAdresseEnvoiDeCommande;
	}

	public void setnAdresseEnvoiDeCommande(String nAdresseEnvoiDeCommande) {
		this.nAdresseEnvoiDeCommande = nAdresseEnvoiDeCommande;
	}

	public String getPn() {
		return pn;
	}

	public void setPn(String pn) {
		this.pn = pn;
	}

	public String getTauxDeRemiseAccordee() {
		return tauxDeRemiseAccordee;
	}

	public void setTauxDeRemiseAccordee(String tauxDeRemiseAccordee) {
		this.tauxDeRemiseAccordee = tauxDeRemiseAccordee;
	}

	public String getType_de_Travail() {
		return type_de_Travail;
	}

	public void setType_de_Travail(String type_de_Travail) {
		this.type_de_Travail = type_de_Travail;
	}

	public Contrat(String nDeContrat, String codetiers, Date dateDeDebutDeContrat, Date dateDeFinDeContrat,
			String delaiAOG, String delaiCritique, String delaiNormal, String designation, String libelleContrat,
			String montantDuContrat, String nAdresseEnvoiDeCommande, String pn, String tauxDeRemiseAccordee,
			String type_de_Travail) {
		super();
		this.nDeContrat = nDeContrat;
		this.codetiers = codetiers;
		this.dateDeDebutDeContrat = dateDeDebutDeContrat;
		this.dateDeFinDeContrat = dateDeFinDeContrat;
		this.delaiAOG = delaiAOG;
		this.delaiCritique = delaiCritique;
		this.delaiNormal = delaiNormal;
		this.designation = designation;
		this.libelleContrat = libelleContrat;
		this.montantDuContrat = montantDuContrat;
		this.nAdresseEnvoiDeCommande = nAdresseEnvoiDeCommande;
		this.pn = pn;
		this.tauxDeRemiseAccordee = tauxDeRemiseAccordee;
		this.type_de_Travail = type_de_Travail;
	}

	public Contrat() {
		super();
		// TODO Auto-generated constructor stub
	}

}