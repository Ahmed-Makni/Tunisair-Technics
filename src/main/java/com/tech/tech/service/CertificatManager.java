package com.tech.tech.service;

import java.util.List;

import com.tech.tech.entites.Certificat;

public interface CertificatManager {
	public String ajouterCertificat(Certificat cert);
	public void modifierCertificat(Certificat cert);
	public void supprimerCertificat(String codecert);
	public Certificat getCertificat(String codecert);
	public List<Certificat> listeCertificat();
}
