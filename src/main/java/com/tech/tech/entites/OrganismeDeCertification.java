package com.tech.tech.entites;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="organismedecertification")
public class OrganismeDeCertification implements Serializable {
	@Id
	private String codeOrganisme;
	@OneToMany
	@JoinColumn(name="codeOrganisme")
	private Collection<Certificat> certificat;
	private String libelleOrganisme;
	public String getCodeOrganisme() {
		return codeOrganisme;
	}
	public void setCodeOrganisme(String codeOrganisme) {
		this.codeOrganisme = codeOrganisme;
	}
	public Collection<Certificat> getCertificat() {
		return certificat;
	}
	public void setCertificat(Collection<Certificat> certificat) {
		this.certificat = certificat;
	}
	public String getLibelleOrganisme() {
		return libelleOrganisme;
	}
	public void setLibelleOrganisme(String libelleOrganisme) {
		this.libelleOrganisme = libelleOrganisme;
	}
	public OrganismeDeCertification() {
		super();
		// TODO Auto-generated constructor stub
	}
	public OrganismeDeCertification(String codeOrganisme, String libelleOrganisme) {
		super();
		this.codeOrganisme = codeOrganisme;
		this.libelleOrganisme = libelleOrganisme;
	}

}
