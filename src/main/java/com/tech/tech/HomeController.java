package com.tech.tech;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application home page.
 */
@Controller

public class HomeController {
	
//	@RequestMapping(value = "/login", method = RequestMethod.GET)
//	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
//			@RequestParam(value = "logout", required = false) String logout) {
//
//		ModelAndView model = new ModelAndView();
//		if (error != null) {
//			model.addObject("error", "Invalid username and password!");
//		}
//
//		if (logout != null) {
//			model.addObject("msg", "You've been logged out successfully.");
//		}
//		model.setViewName("login");
//
//		return model;
//
//	}
	
	
	@RequestMapping(value="/", method = RequestMethod.GET)
	public String login(ModelMap model)
	{
		return "login";
	}
	
	@RequestMapping(value="/accueil", method = RequestMethod.GET)
	public String accueil(ModelMap model)
	{
		return "accueil";
	}
	
	
	
	
	
	
	
	
	
}

