package com.tech.tech.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Certificat implements Serializable{
	@Id
	private String codeCertificat;	
	private String libelleCertificat;
	private String dateD_attribution;
	private String dateD_expiration;
	
	private String codeOrganisme;
	public String getCodeCertificat() {
		return codeCertificat;
	}
	public void setCodeCertificat(String codeCertificat) {
		this.codeCertificat = codeCertificat;
	}
	public String getLibelleCertificat() {
		return libelleCertificat;
	}
	public void setLibelleCertificat(String libelleCertificat) {
		this.libelleCertificat = libelleCertificat;
	}
	public String getDateD_attribution() {
		return dateD_attribution;
	}
	public void setDateD_attribution(String dateD_attribution) {
		this.dateD_attribution = dateD_attribution;
	}
	public String getDateD_expiration() {
		return dateD_expiration;
	}
	public void setDateD_expiration(String dateD_expiration) {
		this.dateD_expiration = dateD_expiration;
	}
	public String getCodeOrganisme() {
		return codeOrganisme;
	}
	public void setCodeOrganisme(String codeOrganisme) {
		this.codeOrganisme = codeOrganisme;
	}
	public Certificat() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Certificat(String codeCertificat, String libelleCertificat, String dateD_attribution,
			String dateD_expiration, String codeOrganisme) {
		super();
		this.codeCertificat = codeCertificat;
		this.libelleCertificat = libelleCertificat;
		this.dateD_attribution = dateD_attribution;
		this.dateD_expiration = dateD_expiration;
		this.codeOrganisme = codeOrganisme;
	}
	
}
