package com.tech.tech.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tech.tech.entites.Utilisateur;
import com.tech.tech.service.IutilisateurMetier;

@Controller
@RequestMapping(value="adminUtilisateur")
public class UtilisateurController {
	
	@Autowired
	IutilisateurMetier metierutilisateur;
	
	@RequestMapping(value = "/index")
	public String index(Model model)
	{
		model.addAttribute("utilisateur", new Utilisateur());
		return "addUser";
	}	
	
	/****Ajout d'un client******/
	@RequestMapping(value="/saveUser")
	public String ajoutClient( @Valid Utilisateur u,BindingResult bind,Model model)
	{
		if(bind.hasErrors())
		{
			return "addUser";
		}
		else
		{
			metierutilisateur.ajouterUtilisateur(u);
			model.addAttribute("utilisateur", new Utilisateur());
			return "addUser";
		}
		
	}
	
	
	
	
}
