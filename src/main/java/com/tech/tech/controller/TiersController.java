package com.tech.tech.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tech.tech.entites.Tiers;
import com.tech.tech.service.TiersManager;


@Controller
@RequestMapping(value="adminTiers")
public class TiersController {
	
	

	
	 @Autowired
		TiersManager tiersManager;
		
	 
		@RequestMapping(value = "/ajout")
		public String home(Model model) {
			
			model.addAttribute("tiers",new Tiers());
			return "addTiers";
		}
		
		/****Ajout d'un client******/
		@RequestMapping(value="/saveTiers")
		public String ajoutTiers( @Valid Tiers t,BindingResult bind,Model model)
		{
			
			if(bind.hasErrors())
			{
		
				return "addTiers";
				
			}
			else
			{
				tiersManager.ajouterTiers(t);
				model.addAttribute("listeTiers",tiersManager.listeTiers());
				return "listeTiers";
			}
			
		}
		
		/**********Suppression fournisseurs****************/
		
		@RequestMapping(value="/supprimerTiers")
		public String suppPro(String refTiers,Model model)
		{  try{ 
	       tiersManager.supprimerTiers(refTiers);
		}catch(Exception e){}
	   	model.addAttribute("listeTiers",tiersManager.listeTiers());
		return "listeTiers";

		}
		
		/******Affichage liste des clients************/
		@RequestMapping(value="liste")
		public String accueil(Model model)
		{
			model.addAttribute("listeTiers",tiersManager.listeTiers());
			return "listeTiers";
		}
		
		
		


}
