package com.tech.tech.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.tech.tech.dao.IutilisateurDao;
import com.tech.tech.entites.Utilisateur;


@Transactional
public class UtilisateurMetierImpl implements IutilisateurMetier{

	IutilisateurDao iUtilisateur;
	@Override
	public String ajouterUtilisateur(Utilisateur u) {
		// TODO Auto-generated method stub
		return iUtilisateur.ajouterUtilisateur(u);
	}

	@Override
	public void modifierUtilisateur(Utilisateur u) {
		iUtilisateur.modifierUtilisateur(u);
	}

	@Override
	public void supprimerUtilisateur(String referenceUtilisateur) {
		iUtilisateur.supprimerUtilisateur(referenceUtilisateur);
	}

	@Override
	public Utilisateur getUtilisateur(String referenceUtilisateur) {
		// TODO Auto-generated method stub
		return iUtilisateur.getUtilisateur(referenceUtilisateur);
	}

	@Override
	public List<Utilisateur> listeUtilisateurs() {
		// TODO Auto-generated method stub
		return iUtilisateur.listeUtilisateurs();
	}

	public IutilisateurDao getUtilisateurDao() {
		return iUtilisateur;
	}

	public void setUtilisateurDao(IutilisateurDao iUtilisateur) {
		this.iUtilisateur = iUtilisateur;
	}

}
