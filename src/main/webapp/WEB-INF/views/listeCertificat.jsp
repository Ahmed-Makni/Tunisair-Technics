<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="j"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tunisair Technics</title>
<!-- Bootstrap Core CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link
	href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link
	href="<%=request.getContextPath()%>/resources/css/metisMenu.min.css"
	rel="stylesheet">

<!-- Timeline CSS -->
<link href="<%=request.getContextPath()%>/resources/css/timeline.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="<%=request.getContextPath()%>/resources/css/startmin.css"
	rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="<%=request.getContextPath()%>/resources/css/morris.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="<%=request.getContextPath()%>/resources/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<link rel="icon" type="image/png"
	href="<%=request.getContextPath()%>/resources/img/favicon.png" />
</head>
<body>
	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-inverse navbar-fixed-top couleur"
			role="navigation">
		<div class="navbar-header">
			<a class="navbar-brand" href="#"><img
				src="<%=request.getContextPath()%>/resources/img/logoo.png"
				class="logo" /></a>
		</div>

		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>

		<!-- Top Navigation: Left Menu --> <!-- Top Navigation: Right Menu -->
	      <ul class="nav navbar-right navbar-top-links">
           
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: white;">
                    <i class="fa fa-user fa-fw"></i> Utilisateur <b class="caret"></b>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i>Profile</a>
                    </li>
                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Paramètres</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="/tech/"><i class="fa fa-sign-out fa-fw"></i> Déconnexion</a>
                    </li>
                </ul>
            </li>
        </ul>

		<!-- Sidebar -->
			<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse">

			<ul class="nav" id="side-menu">


					<li><a href="#" class="active"><i
							class="fa fa-dashboard fa-fw"></i> Accueil</a></li>
					<li><a href=""><i class="fa fa-sitemap fa-fw"></i>
							Dashbord<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="http://localhost:8080/tech/adminTiers/liste">Liste
									des Tiers</a></li>
										<li><a href="http://localhost:8080/tech/adminTiers/liste">Gestion
									des Contrat</a></li>
										<li><a href="http://localhost:8080/tech/adminTiers/liste">Gestion
									des Capabilité liste</a></li>
										<li><a href="http://localhost:8080/tech/adminTiers/liste">Gestion
									des price Catalogue</a></li>
										<li><a href="http://localhost:8080/tech/adminTiers/liste">Gestion
									des litige</a></li>
							
						</ul></li>
				</ul>

			</div>
		</div>
		</nav>

		<!--Contenu de la page-->
		<div id="page-wrapper">
			<div class="container-fluid">

				<div class="row">
					<div class="col-lg-12" style=" margin-top: 15px;">
						<h1 class="page-header">Tite de la page</h1>
						<div>
											<form role="search" name="searchform" method="get">
    <div class="row">
        <div class="col-sm-8">
            <div class="form-group">
                <div class="input-group">
                    <input type="text" placeholder="recherche" name="s" class="form-control left-rounded">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-inverse right-rounded">Chercher</button>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </form>
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Code Certificat</th>
					<th>Libiler Certificat</th>
					<th>Date d'attribution</th>
					<th>date d'expiration</th>
				</tr>
			</thead>

			<j:forEach items="${listeCertificat}" var="emcertificat">
				<tbody>
					<tr>
						<td>${emcertificat.codeCertificat}</td>
						<td>${emcertificat.libelleCertificat}</td>
						<td>${emcertificat.dateD_attribution}</td>
						<td>${emcertificat.dateD_expiration}</td>
						
						<td><a href="supprimerCertificat?refcodecert=${emcertificat.codeCertificat}"> <span class="glyphicon glyphicon-trash"></span></a>
          <td><a href="EditerCertificat?refcodecert=${emcertificat.codeCertificat}"><span class="glyphicon glyphicon-pencil"></span></a>
          

					</tr>
				
				</tbody>
			</j:forEach>

		<td><a href="http://localhost:8080/tech/adminCertificat/ajout"><i
				class="fa fa-angle-double-right"></i> <span class="glyphicon glyphicon-plus"></span></a></td>
		</table>

   

		

		<li><a href="http://localhost:8080/tech/accueil"><i
				class="fa fa-angle-double-right"></i> Dashbord</a></li>
	</div>
					</div>
				</div>

				<!-- ... Fin du Contenu ... -->

			</div>
			<div class="container">
  <h2>Dynamic Pills</h2>
  <p>To make the tabs toggleable, add the data-toggle="pill" attribute to each link. Then add a .tab-pane class with a unique ID for every tab and wrap them inside a div element with class .tab-content.</p>
  <ul class="nav nav-pills">
    <li class="active"><a data-toggle="pill" href="#home">Home</a></li>
    <li><a data-toggle="pill" href="#menu1">Menu 1</a></li>
    <li><a data-toggle="pill" href="#menu2">Menu 2</a></li>
    <li><a data-toggle="pill" href="#menu3">Menu 3</a></li>
  </ul>
  
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <h3>HOME</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
    </div>
    <div id="menu1" class="tab-pane fade">
      <h3>Menu 1</h3>
      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>
    <div id="menu2" class="tab-pane fade">
      <h3>Menu 2</h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
    </div>
    <div id="menu3" class="tab-pane fade">
      <h3>Menu 3</h3>
      <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
    </div>
  </div>
</div>
			
		</div>

	</div>

	<!-- jQuery -->
	<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/js/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="<%=request.getContextPath()%>/resources/js/startmin.js"></script>
</body>
</html>