<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="j"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tunisair Technics</title>
<!-- Bootstrap Core CSS -->
<link
	href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link
	href="<%=request.getContextPath()%>/resources/css/metisMenu.min.css"
	rel="stylesheet">

<!-- Timeline CSS -->
<link href="<%=request.getContextPath()%>/resources/css/timeline.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="<%=request.getContextPath()%>/resources/css/startmin.css"
	rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="<%=request.getContextPath()%>/resources/css/morris.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="<%=request.getContextPath()%>/resources/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<link rel="icon" type="image/png"
	href="<%=request.getContextPath()%>/resources/img/favicon.png" />
</head>
<body>
	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-inverse navbar-fixed-top couleur"
			role="navigation">
		<div class="navbar-header">
			<a class="navbar-brand" href="#"><img
				src="<%=request.getContextPath()%>/resources/img/logoo.png"
				class="logo" /></a>
		</div>

		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>

		<!-- Top Navigation: Left Menu --> <!-- Top Navigation: Right Menu -->
	      <ul class="nav navbar-right navbar-top-links">
           
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: white;">
                    <i class="fa fa-user fa-fw"></i> Utilisateur <b class="caret"></b>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i>Profile</a>
                    </li>
                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Param�tres</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="/tech/"><i class="fa fa-sign-out fa-fw"></i> D�connexion</a>
                    </li>
                </ul>
            </li>
        </ul>

		<!-- Sidebar -->
			<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse">

		<ul class="nav" id="side-menu">


					<li><a href="#" class="active"><i
							class="fa fa-dashboard fa-fw"></i> Accueil</a></li>
					<li><a href=""><i class="fa fa-sitemap fa-fw"></i>
							Dashbord<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="http://localhost:8080/tech/adminTiers/liste">Liste
									des Tiers</a></li>
										<li><a href="http://localhost:8080/tech/adminContrat/liste">Gestion
									des Contrat</a></li>
										<li><a href="http://localhost:8080/tech/adminTiers/liste">Gestion
									des Capabilit� liste</a></li>
										<li><a href="http://localhost:8080/tech/adminTiers/liste">Gestion
									des price Catalogue</a></li>
										<li><a href="http://localhost:8080/tech/adminTiers/liste">Gestion
									des litige</a></li>
							
						</ul></li>
				</ul>


			</div>
		</div>
		</nav>

		<!--Contenu de la page-->
		<div id="page-wrapper">
			<div class="container-fluid">

				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Ajout Contrat</h1>
						<div>
						
					<f:form modelAttribute="contrat" action="saveContrat" method="post">
					
								<div class="box-body">
									<!-- text input -->
									<div class="form-group">
										<label>N� de Contrat</label>
										<f:input path="nDeContrat" class="form-control"
											placeholder="Enter ..." />
										<f:errors path="nDeContrat" cssClass="errors"></f:errors>

									</div>
									<div class="form-group">
										<label>Code Tiers</label>
										<f:input path="codetiers" class="form-control"
											placeholder="Enter ..." />
										<f:errors path="codetiers" cssClass="errors"></f:errors>
									</div>
									<div class="form-group">
										<label>Date de D�but de Contrat</label>
										<f:input type="date" path="dateDeDebutDeContrat" class="form-control"
											placeholder="Enter ..." />
										<f:errors path="dateDeDebutDeContrat" cssClass="errors"></f:errors>
									</div>


									<div class="form-group">
										<label>Date de Fin de Contrat</label>
										<f:input type="date" path="dateDeFinDeContrat" class="form-control"
											placeholder="Enter ..." />
										<f:errors path="dateDeFinDeContrat" cssClass="errors"></f:errors>
									</div>
									<div class="form-group">
										<label>delai AOG</label>
										<f:input path="delaiAOG" class="form-control"
											placeholder="Enter ..." />
										<f:errors path="delaiAOG" cssClass="errors"></f:errors>
									</div>
									<div class="form-group">
										<label>Delai Critique</label>
										<f:input path="delaiCritique" class="form-control"
											placeholder="Enter ..." />
										<f:errors path="delaiCritique" cssClass="errors"></f:errors>
									</div>
									<div class="form-group">
										<label>Delai Normal</label>
										<f:input path="delaiNormal" class="form-control"
											placeholder="Enter ..." />
										<f:errors path="delaiNormal" cssClass="errors"></f:errors>
									</div>
									<div class="form-group">
										<label>Designation</label>
										<f:input path="designation" class="form-control"
											placeholder="Enter ..." />
										<f:errors path="designation" cssClass="errors"></f:errors>
									</div>
									<div class="form-group">
										<label>Libell� Contrat</label>
										<f:input path="libelleContrat" class="form-control"
											placeholder="Enter ..." />
										<f:errors path="libelleContrat" cssClass="errors"></f:errors>
									</div>
									<div class="form-group">
										<label>Montant Du Contrat</label>
										<f:input path="montantDuContrat" class="form-control"
											placeholder="Enter ..." />
										<f:errors path="montantDuContrat" cssClass="errors"></f:errors>
									</div>
									<div class="form-group">
										<label>Adresse 3nvoie De Commande</label>
										<f:input path="nAdresseEnvoiDeCommande" class="form-control"
											placeholder="Enter ..." />
										<f:errors path="nAdresseEnvoiDeCommande" cssClass="errors"></f:errors>
									</div>
									<div class="form-group">
										<label>Part Number</label>
										<f:input path="pn" class="form-control"
											placeholder="Enter ..." />
										<f:errors path="pn" cssClass="errors"></f:errors>
									</div>
									<div class="form-group">
										<label>Taux de Remise Accord�</label>
										<f:input path="tauxDeRemiseAccordee" class="form-control"
											placeholder="Enter ..." />
										<f:errors path="tauxDeRemiseAccordee" cssClass="errors"></f:errors>
									</div>
									<div class="form-group">
										<label>Type de Travail</label>
										<f:input path="type_de_Travail" class="form-control"
											placeholder="Enter ..." />
										<f:errors path="type_de_Travail" cssClass="errors"></f:errors>
									</div>

									<div class="box-footer">
										<button type="submit" class="btn btn-primary">Submit</button>
									</div>

								</div>
								<!-- /.box-body -->
							</f:form>
					


						</div>
					</div>
				</div>

				<!-- ... Fin du Contenu ... -->

			</div>
		</div>

	</div>

	<!-- jQuery -->
	<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/js/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="<%=request.getContextPath()%>/resources/js/startmin.js"></script>
</body>
</html>