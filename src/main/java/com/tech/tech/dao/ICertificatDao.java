package com.tech.tech.dao;

import java.util.List;

import com.tech.tech.entites.Certificat;

public interface ICertificatDao {
	public String ajouterCertificat(Certificat cert);
	public void modifierCertificat(Certificat cert);
	public void supprimerCertificat(String codecert);
	public Certificat getCertificat(String codecert);
	public List<Certificat> listeCertificat();
}
