package com.tech.tech.dao;

import java.util.List;

import com.tech.tech.entites.Contrat;


public interface IContratDao {
	public String ajouterContrat(Contrat cert);
	public void modifierContrat(Contrat cert);
	public void supprimerContrat(String codecert);
	public Contrat getContrat(String codecert);
	public List<Contrat> listeContrat();
}
