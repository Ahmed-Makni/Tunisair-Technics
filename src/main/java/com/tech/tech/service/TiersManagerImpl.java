package com.tech.tech.service;

import java.util.List;

import javax.persistence.Query;

import com.tech.tech.dao.ITiersDao;
import com.tech.tech.entites.Tiers;

public class TiersManagerImpl implements TiersManager {
	
	ITiersDao iTiersDao;

	@Override
	public String ajouterTiers(Tiers t) {
		iTiersDao.ajouterTiers(t);
		return t.getCodeTier();
	}

	@Override
	public void modifierTiers(Tiers t) {
		iTiersDao.modifierTiers(t);;
	}

	@Override
	public void supprimerTiers(String t) {
		iTiersDao.supprimerTiers(t);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tiers> listeTiers() {
		return iTiersDao.listeTiers();
	}

	public Tiers getTiers(Tiers t) {
		return iTiersDao.getTiers(t);
	}

	public ITiersDao getTiersDao() {
		return iTiersDao;
	}

	public void setTiersDao(ITiersDao iTiersDao) {
		this.iTiersDao = iTiersDao;
	}

}
