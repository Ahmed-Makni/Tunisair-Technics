<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="j"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tunisair Technics</title>
<!-- Bootstrap Core CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link
	href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link
	href="<%=request.getContextPath()%>/resources/css/metisMenu.min.css"
	rel="stylesheet">

<!-- Timeline CSS -->
<link href="<%=request.getContextPath()%>/resources/css/timeline.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="<%=request.getContextPath()%>/resources/css/startmin.css"
	rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="<%=request.getContextPath()%>/resources/css/morris.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="<%=request.getContextPath()%>/resources/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<link rel="icon" type="image/png"
	href="<%=request.getContextPath()%>/resources/img/favicon.png" />
</head>
<body>
	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-inverse navbar-fixed-top couleur"
			role="navigation">
		<div class="navbar-header">
			<a class="navbar-brand" href="#"><img
				src="<%=request.getContextPath()%>/resources/img/logoo.png"
				class="logo" /></a>
		</div>

		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>

		<!-- Top Navigation: Left Menu --> <!-- Top Navigation: Right Menu -->
	      <ul class="nav navbar-right navbar-top-links">
           
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: white;">
                    <i class="fa fa-user fa-fw"></i> Utilisateur <b class="caret"></b>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i>Profile</a>
                    </li>
                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Param�tres</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="/tech/"><i class="fa fa-sign-out fa-fw"></i> D�connexion</a>
                    </li>
                </ul>
            </li>
        </ul>
</div>
		<!-- Sidebar -->
			<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse">

			<ul class="nav" id="side-menu">


					<li><a href="#" class="active"><i
							class="fa fa-dashboard fa-fw"></i> Accueil</a></li>
					<li><a href=""><i class="fa fa-sitemap fa-fw"></i>
							Dashbord<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="http://localhost:8080/tech/adminTiers/liste">Liste
									des Tiers</a></li>
										<li><a href="http://localhost:8080/tech/adminTiers/liste">Gestion
									des Contrat</a></li>
										<li><a href="http://localhost:8080/tech/adminTiers/liste">Gestion
									des Capabilit� liste</a></li>
										<li><a href="http://localhost:8080/tech/adminTiers/liste">Gestion
									des price Catalogue</a></li>
										<li><a href="http://localhost:8080/tech/adminTiers/liste">Gestion
									des litige</a></li>
							
						</ul></li>
				</ul>


			</div>
		</div>
		</nav>

		<!--Contenu de la page-->
		<div id="page-wrapper">
			<div class="container-fluid">

				<div class="row">
					<div class="col-lg-12" style=" margin-top: 15px;">
						<h1 class="page-header">Gestion Des Contrat</h1>
						<div>
											<form role="search" name="searchform" method="get">
    <div class="row">
        <div class="col-sm-8">
            <div class="form-group">
                <div class="input-group">
                    <input type="text" placeholder="recherche" name="s" class="form-control left-rounded">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-inverse right-rounded">Chercher</button>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </form>
		
					</div>
				</div>
		</div>
			<div class="container">
			
			<table id="example1" class="table table-hover">
			<thead>
				<tr>
					<th>n�de Contrat</th>
					<th>Date de D�but de contrat </th>
					<th>date de fin de contrat </th>
					<th>libelle Contrat</th>
					<th>Montant de Contrat</th>
					<th>Taux de remise Accordee</th>
				
				</tr>
			</thead>

			<j:forEach items="${listeContrat}" var="emcontrat">
				<tbody>
					<tr>
						<td>${emcontrat.nDeContrat}</td>
						<td>${emcontrat.dateDeDebutDeContrat}</td>
						<td>${emcontrat.dateDeFinDeContrat}</td>
						<td>${emcontrat.libelleContrat}</td>
						<td>${emcontrat.montantDuContrat}</td>
						<td>${emcontrat.tauxDeRemiseAccordee}</td>
						
						<td><a href="supprimerContrat?refcodeContrat=${emcontrat.nDeContrat}"> <span class="glyphicon glyphicon-trash"></span></a>
          <td><a href="EditerContrat?refcodeContrat=${emcontrat.nDeContrat}"><span class="glyphicon glyphicon-pencil"></span></a>

					</tr>
				</tbody>
			</j:forEach>

	<td><a href="http://localhost:8080/tech/adminContrat/ajout"><i
				class="fa fa-angle-double-right"></i> <span class="glyphicon glyphicon-plus"></span></a></td>
		</table>
		<div class="container">
  <h2>Contrat</h2>
  <ul class="nav nav-pills">
    <li class="active"><a data-toggle="pill" href="#home">D�tail Contrat</a></li>
    <li><a data-toggle="pill" href="#menu1">D�tail Pi�ces</a></li>

  </ul>
   <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <h3>D�tail Contrat</h3>
    	<table id="example1" class="table table-hover">
			<thead>
				<tr>
					<th>Adresse Envoie De Commande</th>
					<th>Type de Travail</th>
						<th>Code Tiers</th>
				</tr>
			</thead>

			<j:forEach items="${listeContrat}" var="emcontrat">
				<tbody>
					<tr>
						<td>${emcontrat.nAdresseEnvoiDeCommande}</td>
						<td>${emcontrat.type_de_Travail}</td>
						<td>${emcontrat.codetiers}</td>

					</tr>
				</tbody>
			</j:forEach>

	
		</table>
    </div>
    <div id="menu1" class="tab-pane fade">
      <h3>D�tail Pi�ces</h3>
      <table id="example1" class="table table-hover">
			<thead>
				<tr>
					<th>Delai Normal</th>
					<th>Delai Critique </th>
					<th>Delai AOG</th>
				</tr>
			</thead>

			<j:forEach items="${listeContrat}" var="emcontrat">
				<tbody>
					<tr>
					<td>${emcontrat.delaiNormal}</td>
						<td>${emcontrat.delaiCritique}</td>
						<td>${emcontrat.delaiAOG}</td>
					</tr>
				</tbody>
			</j:forEach>
		</table>
			
    </div>


  </div>
</div>
</div>
</div>
</div>
				<!-- ... Fin du Contenu ... -->

	

	<!-- jQuery -->
	<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/js/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="<%=request.getContextPath()%>/resources/js/startmin.js"></script>
</body>
</html>