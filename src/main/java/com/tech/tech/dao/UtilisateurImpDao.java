package com.tech.tech.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.tech.tech.entites.Utilisateur;



public class UtilisateurImpDao implements IutilisateurDao {
	@PersistenceContext
	EntityManager em;
	@Override
	public String ajouterUtilisateur(Utilisateur u) {
    em.persist(u);
     return u.getMatricule();
	}

	@Override
	public void modifierUtilisateur(Utilisateur u) {
    em.merge(u);		
	}

	@Override
	public void supprimerUtilisateur(String referenceUtilisateur) {
    Utilisateur u=em.find(Utilisateur.class, referenceUtilisateur);
    em.remove(u);
		
	}

	@Override
	public Utilisateur getUtilisateur(String referenceUtilisateur) {
		// TODO Auto-generated method stub
		return em.find(Utilisateur.class, referenceUtilisateur);
	}

	@Override
	public List<Utilisateur> listeUtilisateurs() {
		Query q=em.createQuery("select  u from Utilisateur u ")	;	
		
		return q.getResultList();
	}

}
