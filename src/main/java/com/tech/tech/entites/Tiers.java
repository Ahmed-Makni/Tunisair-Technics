package com.tech.tech.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Tiers implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	   private String codeTier;
	
	   private String nomTier;
	 
	   private String adressTier;
	
	   private int tat;

	private int nDemandeCotation;
	private int codeEvaluation;
	private  String	dateEvaluation;
	private   String	DTYPE;
	private String codeSousTraitant;
	public String getCodeTier() {
		return codeTier;
	}
	public void setCodeTier(String codeTier) {
		this.codeTier = codeTier;
	}
	public String getNomTier() {
		return nomTier;
	}
	public void setNomTier(String nomTier) {
		this.nomTier = nomTier;
	}
	public String getAdressTier() {
		return adressTier;
	}
	public void setAdressTier(String adressTier) {
		this.adressTier = adressTier;
	}
	public int getTat() {
		return tat;
	}
	public void setTat(int tat) {
		this.tat = tat;
	}
	public int getnDemandeCotation() {
		return nDemandeCotation;
	}
	public void setnDemandeCotation(int nDemandeCotation) {
		this.nDemandeCotation = nDemandeCotation;
	}
	public int getCodeEvaluation() {
		return codeEvaluation;
	}
	public void setCodeEvaluation(int codeEvaluation) {
		this.codeEvaluation = codeEvaluation;
	}
	public String getDateEvaluation() {
		return dateEvaluation;
	}
	public void setDateEvaluation(String dateEvaluation) {
		this.dateEvaluation = dateEvaluation;
	}
	public String getDTYPE() {
		return DTYPE;
	}
	public void setDTYPE(String dTYPE) {
		DTYPE = dTYPE;
	}
	public String getCodeSousTraitant() {
		return codeSousTraitant;
	}
	public void setCodeSousTraitant(String codeSousTraitant) {
		this.codeSousTraitant = codeSousTraitant;
	}
	public Tiers(String codeTier, String nomTier, String adressTier, int tat, int nDemandeCotation, int codeEvaluation,
			String dateEvaluation, String dTYPE, String codeSousTraitant) {
		super();
		this.codeTier = codeTier;
		this.nomTier = nomTier;
		this.adressTier = adressTier;
		this.tat = tat;
		this.nDemandeCotation = nDemandeCotation;
		this.codeEvaluation = codeEvaluation;
		this.dateEvaluation = dateEvaluation;
		DTYPE = dTYPE;
		this.codeSousTraitant = codeSousTraitant;
	}
	public Tiers() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
