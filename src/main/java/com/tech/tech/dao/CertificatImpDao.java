package com.tech.tech.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.tech.tech.entites.Certificat;

public class CertificatImpDao implements ICertificatDao{
	@PersistenceContext
	EntityManager emcertificat;
	@Transactional
	@Override
	public String ajouterCertificat(Certificat cert) {
		// TODO Auto-generated method stub
		Certificat cer=new Certificat(cert.getCodeCertificat(), cert.getLibelleCertificat(),cert.getDateD_attribution(),cert.getDateD_expiration(),cert.getCodeOrganisme());
		emcertificat.persist(cer);
		return cer.getCodeCertificat();
	}
	@Transactional
	@Override
	public void modifierCertificat(Certificat cert) {
		// TODO Auto-generated method stub
		emcertificat.merge(cert);
	}
	@Transactional
	@Override
	public void supprimerCertificat(String codecert) {
		// TODO Auto-generated method stub
		try{
			Certificat cer = emcertificat.find(Certificat.class, codecert);
			emcertificat.remove(cer);
		} catch(Exception e){}
	}
	
	
	@Transactional
	@Override
	public Certificat getCertificat(String codecert) {
		// TODO Auto-generated method stub
		return emcertificat.find(Certificat.class, codecert);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Certificat> listeCertificat() {
		// TODO Auto-generated method stub
		Query req=emcertificat.createQuery("from Certificat");
		return req.getResultList();
	}

}
