package com.tech.tech.dao;

import java.util.List;

import com.tech.tech.entites.Tiers;


public interface ITiersDao {
	public String ajouterTiers(Tiers t);
	public void modifierTiers(Tiers t);
	public void supprimerTiers(String t);
	public Tiers getTiers(Tiers t);
	public List<Tiers> listeTiers();

}
