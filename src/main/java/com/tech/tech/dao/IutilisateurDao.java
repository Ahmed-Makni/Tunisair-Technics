package com.tech.tech.dao;

import java.util.List;

import com.tech.tech.entites.Utilisateur;



public interface IutilisateurDao {
	public String ajouterUtilisateur(Utilisateur u);
	public void modifierUtilisateur(Utilisateur u);
	public void supprimerUtilisateur(String referenceUtilisateur);
	public Utilisateur getUtilisateur(String referenceUtilisateur);
	public  List<Utilisateur> listeUtilisateurs();
}
