package com.tech.tech.service;

import java.util.List;

import com.tech.tech.entites.Tiers;

public interface TiersManager {
	
	public String ajouterTiers(Tiers t);
	public void modifierTiers(Tiers t);
	public void supprimerTiers(String t);
	public Tiers getTiers(Tiers t);
	public List<Tiers> listeTiers();

}
