package com.tech.tech.dao;

import java.util.List;

import com.tech.tech.entites.Commande;


public interface ICommandeDao {
	public String ajouterCommande(Commande c);
	public void modifierCommande(Commande c);
	public void supprimerCommande(String c);
	public Commande getCommande(String c);
	public List<Commande> listeCommande();
}
