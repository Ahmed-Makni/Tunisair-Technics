package com.tech.tech.service;

import java.util.List;

import com.tech.tech.dao.ICommandeDao;
import com.tech.tech.entites.Commande;

public class CommandeManagerImp implements CommandeManager{
ICommandeDao icommandeDao;
	@Override
	public String ajouterCommande(Commande c) {
		// TODO Auto-generated method stub
		icommandeDao.ajouterCommande(c);
		return c.getnCommande();
	}

	@Override
	public void modifierCommande(Commande c) {
		// TODO Auto-generated method stub
		icommandeDao.modifierCommande(c);
		
	}

	@Override
	public void supprimerCommande(String c) {
		// TODO Auto-generated method stub
		icommandeDao.supprimerCommande(c);
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Commande> listeCommande() {
		// TODO Auto-generated method stub
		return icommandeDao.listeCommande();
	}
	@Override
	public Commande getCommande(String c) {
		// TODO Auto-generated method stub
		return icommandeDao.getCommande(c);
	}
public ICommandeDao getCommandeDao(){
	return icommandeDao;
	
}
	public void setCommandeDao(ICommandeDao icommandeDao){
		this.icommandeDao=icommandeDao;
	}

}
