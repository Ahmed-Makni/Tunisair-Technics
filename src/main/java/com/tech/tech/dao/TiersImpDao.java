package com.tech.tech.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.tech.tech.entites.Tiers;

public class TiersImpDao implements ITiersDao {
	/* Injection des dépendences */
	@PersistenceContext
	EntityManager emtiers;

	@Override
	@Transactional
	public String ajouterTiers(Tiers t) {
//		Query req = emtiers
//				.createNativeQuery("INSERT INTO Tiers(code_tier, code_tier, adress_tier, code_certificat) VALUES ( ?, ?,?,?)");
//		req.executeUpdate();
		Tiers t1 = new Tiers(t.getCodeTier(), t.getNomTier(), t.getAdressTier(), t.getTat(), t.getnDemandeCotation(), t.getCodeEvaluation(), t.getDateEvaluation(), t.getDTYPE(), t.getCodeSousTraitant());
		emtiers.persist(t1); //em.merge(u); for updates
		return t1.getCodeTier();
	}

	@Override
	@Transactional
	public void modifierTiers(Tiers t) {
		emtiers.merge(t);
	}

	@Override
	@Transactional
	public void supprimerTiers(String t) {
		try {
			Tiers t1 = emtiers.find(Tiers.class, t);
			emtiers.remove(t1);
		} catch (Exception e) {
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tiers> listeTiers() {
		Query req = emtiers.createQuery("from Tiers");
		return req.getResultList();
	}

	public Tiers getTiers(Tiers t) {
		return emtiers.find(Tiers.class, t.getCodeTier());
	}
}
