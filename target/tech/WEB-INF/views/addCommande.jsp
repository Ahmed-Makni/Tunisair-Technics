<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="j"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tunisair Technics</title>
<!-- Bootstrap Core CSS -->
<link
	href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link
	href="<%=request.getContextPath()%>/resources/css/metisMenu.min.css"
	rel="stylesheet">

<!-- Timeline CSS -->
<link href="<%=request.getContextPath()%>/resources/css/timeline.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="<%=request.getContextPath()%>/resources/css/startmin.css"
	rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="<%=request.getContextPath()%>/resources/css/morris.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="<%=request.getContextPath()%>/resources/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

<link rel="icon" type="image/png"
	href="<%=request.getContextPath()%>/resources/img/favicon.png" />
</head>
<body>
	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-inverse navbar-fixed-top couleur"
			role="navigation">
		<div class="navbar-header">
			<a class="navbar-brand" href="#"><img
				src="<%=request.getContextPath()%>/resources/img/logoo.png"
				class="logo" /></a>
		</div>

		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>

		<!-- Top Navigation: Left Menu --> <!-- Top Navigation: Right Menu -->
	      <ul class="nav navbar-right navbar-top-links">
           
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: white;">
                    <i class="fa fa-user fa-fw"></i> Utilisateur <b class="caret"></b>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="#"><i class="fa fa-user fa-fw"></i>Profile</a>
                    </li>
                    <li><a href="#"><i class="fa fa-gear fa-fw"></i> Param�tres</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="/tech/"><i class="fa fa-sign-out fa-fw"></i> D�connexion</a>
                    </li>
                </ul>
            </li>
        </ul>

		<!-- Sidebar -->
			<div class="navbar-default sidebar" role="navigation">
			<div class="sidebar-nav navbar-collapse">
<ul class="nav" id="side-menu">


					<li><a href="#" class="active"><i
							class="fa fa-dashboard fa-fw"></i> Accueil</a></li>
					<li><a href=""><i class="fa fa-sitemap fa-fw"></i>
							Dashbord<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">
							<li><a href="http://localhost:8080/tech/adminTiers/liste">Liste
									des Tiers</a></li>
										<li><a href="http://localhost:8080/tech/adminTiers/liste">Gestion
									des Contrat</a></li>
										<li><a href="http://localhost:8080/tech/adminTiers/liste">Gestion
									des Capabilit� liste</a></li>
										<li><a href="http://localhost:8080/tech/adminTiers/liste">Gestion
									des price Catalogue</a></li>
										<li><a href="http://localhost:8080/tech/adminTiers/liste">Gestion
									des litige</a></li>
							
						</ul></li>
				</ul>


			</div>
		</div>
		</nav>

		<!--Contenu de la page-->
		<div id="page-wrapper">
			<div class="container-fluid">

				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Tite de la page</h1>
						<div>
					<f:form modelAttribute="commande" action="saveCommande" method="post">
								<div class="box-body">
									<!-- text input -->
									<div class="form-group">
										<label>N�Commande</label>
										<f:input path="nCommande" class="form-control"
											placeholder="Enter ..." />
										<f:errors path="nCommande" cssClass="errors"></f:errors>

									</div>
									<div class="form-group">
										<label>dateCommande</label>
										<f:input path="dateCommande" class="form-control"
											placeholder="Enter ..." />
										<f:errors path="dateCommande" cssClass="errors"></f:errors>
									</div>
									<div class="form-group">
										<label>typeCommande</label>
										<f:input path="typeCommande" class="form-control"
											placeholder="Enter ..." />
										<f:errors path="typeCommande" cssClass="errors"></f:errors>
									</div>


									<div class="form-group">
										<label>typeOperation</label>
										<f:input path="typeOperation" class="form-control"
											placeholder="Enter ..." />
										<f:errors path="typeOperation" cssClass="errors"></f:errors>
									</div>


									<div class="box-footer">
										<button type="submit" class="btn btn-primary">Submit</button>
									</div>

								</div>
								<!-- /.box-body -->
							</f:form>
					


						</div>
						<li><a href="http://localhost:8080/tech/adminCommande/liste"><i
								class="fa fa-angle-double-right"></i> Liste Commande</a></li>

						<li><a href="http://localhost:8080/tech/accueil"><i
								class="fa fa-angle-double-right"></i> Dashbord</a></li>
					</div>
				</div>

				<!-- ... Fin du Contenu ... -->

			</div>
		</div>

	</div>

	<!-- jQuery -->
	<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/js/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="<%=request.getContextPath()%>/resources/js/startmin.js"></script>
</body>
</html>