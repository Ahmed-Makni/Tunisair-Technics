package com.tech.tech.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tech.tech.entites.Contrat;
import com.tech.tech.service.ContratManager;
import com.tech.tech.service.TiersManager;

@Controller
@RequestMapping(value="adminContrat")
public class ContratController {
	@Autowired
	ContratManager contratManager;
	
	
	@RequestMapping(value= "/ajout")
	public String home(Model model){
		model.addAttribute("contrat",new Contrat());
		return "addContrat";
	}
	@RequestMapping(value="/saveContrat")
	public String ajoutContrat(@Valid Contrat cert,BindingResult bind,Model model)
	{
		
		if(bind.hasErrors())
		{
	
			return "addContrat";
		}
		if((cert.getnDeContrat())!=null){
			contratManager.modifierContrat(cert);
			
			
			}
		else
		
			contratManager.ajouterContrat(cert) ;
			model.addAttribute("listeContrat",contratManager.listeContrat());
			
		
		return "listeContrat";
	}
	@RequestMapping(value="/supprimerContrat")
	public String suppPro(String refcodeContrat,Model model)
	{  try{ 
		contratManager.supprimerContrat(refcodeContrat);
	}catch(Exception e){}
   	model.addAttribute("listeContrat",contratManager.listeContrat());
	return "listeContrat";

	}
	
	@RequestMapping(value="liste")
	public String accueil(Model model)
	{	
		model.addAttribute("listeContrat",contratManager.listeContrat());
		return "listeContrat";
	}
	
	@RequestMapping(value="/EditerContrat")
	public String EditerContrat(String refcodeContrat,Model model)
	{  
	Contrat cert=contratManager.getContrat(refcodeContrat);
		model.addAttribute("contrat",cert);
	return "addContrat";

	}
}
