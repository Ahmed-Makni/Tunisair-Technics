package com.tech.tech.service;

import java.util.List;

import com.tech.tech.dao.IContratDao;
import com.tech.tech.entites.Contrat;

public class ContratManagerImp implements ContratManager {
	
IContratDao iContratDao;
	@Override
	public String ajouterContrat(Contrat cert) {
		 iContratDao.ajouterContrat(cert);
		 return cert.getnDeContrat();
	}

	@Override
	public void modifierContrat(Contrat cert) {
		// TODO Auto-generated method stub
		iContratDao.modifierContrat(cert);
	}
	@SuppressWarnings("unchecked")
	@Override
	public void supprimerContrat(String codecert) {
		// TODO Auto-generated method stub
		iContratDao.supprimerContrat(codecert);
	}

	@Override
	public Contrat getContrat(String codecert) {
		// TODO Auto-generated method stub
		return iContratDao.getContrat(codecert);
	}

	@Override
	public List<Contrat> listeContrat() {
		// TODO Auto-generated method stub
		return iContratDao.listeContrat();
	}

	public IContratDao getContratDao() {
		return iContratDao;
	}

	public void setContratDao(IContratDao iContratDao) {
		this.iContratDao = iContratDao;
	}


}
