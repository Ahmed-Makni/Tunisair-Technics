package com.tech.tech.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.tech.tech.entites.Commande;

public class CommandeImpDao implements ICommandeDao {
	@PersistenceContext
	EntityManager emcommande;
	@Transactional
	public String ajouterCommande(Commande c){
		Commande c1=new Commande(c.getnCommande(), c.getDateCommande(), c.getTypeCommande(), c.getTypeOperation());
		emcommande.persist(c1);
		return c1.getnCommande();
		
	}
	@Override
	@Transactional
	public void modifierCommande(Commande c) {
		// TODO Auto-generated method stub
		emcommande.merge(c);
	}
	@Override
	@Transactional
	public void supprimerCommande(String c) {
		// TODO Auto-generated method stub
		try{
			Commande c1 = emcommande.find(Commande.class, c);
			emcommande.remove(c1);
		} catch(Exception e){}
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Commande> listeCommande() {
		// TODO Auto-generated method stub
		Query req=emcommande.createQuery("from Commande");
		return req.getResultList();
	}
	@Override
	@Transactional
	public Commande getCommande(String c) {
		// TODO Auto-generated method stub
		return emcommande.find(Commande.class, c);
	}

	

}
