package com.tech.tech.entites;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Utilisateur implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private String matricule;
	
	private String nom_complet;
	
	private String mail;
	
	private String userName;
	
	private String password;
	
	private boolean actived;
	
/*L'association n'est pas bidirectionelle donc on sp�cifie cl� �trang�r ici*/
	
	public boolean isActived() {
		return actived;
	}
	public void setActived(boolean actived) {
		this.actived = actived;
	}
//	@OneToMany
//	@JoinColumn(name="userid")
//	private Collection<Role>roles;	
//	
//	@OneToMany(mappedBy="utilisateur")
//	private Collection<Commande> commandes;
//	
//	public Collection<Commande> getCommandes() {
//		return commandes;
//	}
//	public void setCommandes(Collection<Commande> commandes) {
//		this.commandes = commandes;
//	}
	public String getMatricule() {
		return this.matricule;
	}
	public void setMatricule(String mat) {
		this.matricule = mat;
	}
	public String getNomComplet() {
		return nom_complet;
	}
	public void setNomComplet(String nom) {
		this.nom_complet = nom;
	}
	
	
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	
	public String getNom_complet() {
		return nom_complet;
	}
	public void setNom_complet(String nom_complet) {
		this.nom_complet = nom_complet;
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Utilisateur() {
		super();
	}
	
	public Utilisateur(String matricule, String nom_complet,
			String mail, String login, String password,boolean actived) {
		super();
		this.matricule = matricule;
		this.nom_complet = nom_complet;
		
		this.mail = mail;
		this.userName = login;
		this.password = password;
		this.actived=actived;
	}
//	public Collection<Role> getRoles() {
//		return roles;
//	}
//	public void setRoles(Collection<Role> roles) {
//		this.roles = roles;
//	}
//	
	
	
}
