package com.tech.tech.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tech.tech.entites.Certificat;
import com.tech.tech.entites.Commande;
import com.tech.tech.service.CertificatManager;
import com.tech.tech.service.CommandeManager;

@Controller
@RequestMapping(value="adminCertificat")
public class CertificatController {
	@Autowired
	CertificatManager certificatManager;
	@RequestMapping(value= "/ajout")
	public String home(Model model){
		model.addAttribute("certificat",new Certificat());
		return "addCertificat";
	}
	@RequestMapping(value="/saveCertificat")
	public String ajoutCertificat(@Valid Certificat cert,BindingResult bind,Model model)
	{
		
		if(bind.hasErrors())
		{
	
			return "addCertificat";
		}
		if((cert.getCodeCertificat())!=null){
			certificatManager.modifierCertificat(cert);
			
			
			}
		else
		
			certificatManager.ajouterCertificat(cert) ;
			model.addAttribute("listeCertificat",certificatManager.listeCertificat());
			
		
		return "listeCertificat";
	}
	
	@RequestMapping(value="/supprimerCertificat")
	public String suppPro(String refcodecert,Model model)
	{  try{ 
		certificatManager.supprimerCertificat(refcodecert);;
	}catch(Exception e){}
   	model.addAttribute("listeCertificat",certificatManager.listeCertificat());
	return "listeCertificat";

	}
	@RequestMapping(value="liste")
	public String accueil(Model model)
	{
		model.addAttribute("listeCertificat",certificatManager.listeCertificat());
		return "listeCertificat";
	}
	
	@RequestMapping(value="/EditerCertificat")
	public String EditerCertificat(String refcodecert,Model model)
	{  
	Certificat cert=certificatManager.getCertificat(refcodecert);
		model.addAttribute("certificat",cert);
	return "addCertificat";

	}
	
}
