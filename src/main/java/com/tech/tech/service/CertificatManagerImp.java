package com.tech.tech.service;

import java.util.List;

import com.tech.tech.dao.ICertificatDao;
import com.tech.tech.entites.Certificat;

public class CertificatManagerImp implements CertificatManager {
ICertificatDao iCertificatDao;
	@Override
	public String ajouterCertificat(Certificat cert) {
		// TODO Auto-generated method stub
		iCertificatDao.ajouterCertificat(cert);
		return cert.getCodeCertificat();
	}

	@Override
	public void modifierCertificat(Certificat cert) {
		// TODO Auto-generated method stub
		iCertificatDao.modifierCertificat(cert);
	}

	@Override
	public void supprimerCertificat(String codecert) {
		// TODO Auto-generated method stub
		iCertificatDao.supprimerCertificat(codecert);
	}
	@SuppressWarnings("unchecked")
	@Override
	public Certificat getCertificat(String codecert) {
		// TODO Auto-generated method stub
		return iCertificatDao.getCertificat(codecert);
	}

	@Override
	public List<Certificat> listeCertificat() {
		// TODO Auto-generated method stub
		return iCertificatDao.listeCertificat();
		
	}
	public ICertificatDao getCertificatDao(){
		return iCertificatDao;
	}
	public void setCertificatDao (ICertificatDao iCertificatDao){
		this.iCertificatDao=iCertificatDao;
	}

}
