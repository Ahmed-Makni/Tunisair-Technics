package com.tech.tech.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.tech.tech.entites.Contrat;

public class ContratImpDao implements IContratDao{
	@PersistenceContext
	EntityManager emcontrat;
	@Transactional
	@Override
	public String ajouterContrat(Contrat cert) {
	Contrat contrat=new Contrat(cert.getnDeContrat(),cert.getCodetiers(),cert.getDateDeDebutDeContrat(),
			cert.getDateDeFinDeContrat(),cert.getDelaiAOG(),cert.getDelaiCritique(),
			cert.getDelaiNormal(),cert.getDesignation(),cert.getLibelleContrat(),
			cert.getMontantDuContrat(),cert.getnAdresseEnvoiDeCommande(),cert.getPn(),
			cert.getTauxDeRemiseAccordee(),cert.getType_de_Travail());
	emcontrat.persist(contrat);
		return contrat.getnDeContrat();
	}

	@Override
	@Transactional
	public void modifierContrat(Contrat cert) {
		// TODO Auto-generated method stub
		emcontrat.merge(cert);
		
	}

	@Override
	@Transactional
	public void supprimerContrat(String codecert) {
		try{
			Contrat contrat = emcontrat.find(Contrat.class, codecert);
			emcontrat.remove(contrat);
		} catch(Exception e){}
	}
	
	
	@Override
	@Transactional
	public Contrat getContrat(String codecert) {
		// TODO Auto-generated method stub
		return emcontrat.find(Contrat.class, codecert);
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Contrat> listeContrat() {
		// TODO Auto-generated method stub
		Query req=emcontrat.createQuery("from Contrat");
		return req.getResultList();
	}

}
