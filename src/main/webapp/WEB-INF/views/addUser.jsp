<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="j" %>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="f" %>
<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>AdminLTE | Registration Page</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<%=request.getContextPath()%>/resources/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<%=request.getContextPath()%>/resources/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">

        <div class="form-box" id="login-box">
            <div class="header">Ajout d'un utilisateur</div>
             <f:form modelAttribute="utilisateur" action="saveUser" method="post" >
                <div class="body bg-gray">
                    
                    <div class="form-group">
                    <f:input path="matricule" class="form-control" placeholder="Matricule ..."/>
                    <f:errors path="matricule" cssClass="errors"></f:errors>
                    </div>
                    
                    <div class="form-group">
                    <f:input path="nom_complet" class="form-control" placeholder="Nom Complet ..."/>
                    <f:errors path="nom_complet" cssClass="errors"></f:errors>
                    </div>
                    
                    <div class="form-group">
                    <f:input  path="mail"  type="email" class="form-control" placeholder="Mail ..."/>
                    <f:errors path="mail" cssClass="errors"></f:errors>
                    </div>
                    
                    <div class="form-group">
                    <f:input path="login" class="form-control" placeholder="Login ..."/>
                    <f:errors path="login" cssClass="errors"></f:errors>
                    </div>
                                            
                    <div class="form-group">
                     <f:input path="password" type="password" class="form-control" placeholder="Mot de Passe ..."/>
                    <f:errors path="password" cssClass="errors"></f:errors>
					</div>
                </div>
                 <div class="footer">                    
					<button type="submit" class="btn bg-olive btn-block">Enregistrer</button>
				</div>
				
				
             </f:form>

            <div class="margin text-center">
                <span>Register using social networks</span>
                <br/>
                <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
                <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
                <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>
            </div>
        </div>


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js" type="text/javascript"></script>

    </body>
</html>